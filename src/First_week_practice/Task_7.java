package First_week_practice;

import java.util.Scanner;

public class Task_7 {
    public static final double LITTERS_IN_GALLON = 0.219969;
    public static void main(String[] args) {
        //Перевод литров в галлоны
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        double res = n* LITTERS_IN_GALLON;
        System.out.println(res);

    }
}

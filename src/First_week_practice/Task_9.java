package First_week_practice;

import java.util.Scanner;
// Даны целые 3 числа. Нужно вычислить дискриминант
//Формула D = b^2 - 4 * a * c

public class Task_9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a= input.nextInt();
        int b= input.nextInt();
        int c = input.nextInt();
        double discriminant = Math.pow(b,2)-4*a*c;

        System.out.println("discriminant equals:"+discriminant);
    }
}

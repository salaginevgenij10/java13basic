package Home_Work.week_4;
/*
.


Необходимо реализовать игру. Алгоритм игры должен быть записан в
отдельном методе. В методе main должен быть только вызов метода с
алгоритмом игры.

Условия следующие:
Компьютер «загадывает» (с помощью генератора случайных чисел) целое
число M в промежутке от 0 до 1000 включительно. Затем предлагает
пользователю угадать это число. Пользователь вводит число с клавиатуры.
Если пользователь угадал число M, то вывести на экран "Победа!". Если
введенное пользователем число меньше M, то вывести на экран "Это число
меньше загаданного." Если введенное число больше, то вывести "Это число
больше загаданного." Продолжать игру до тех пор, пока число не будет отгадано
или пока не будет введено любое отрицательное число
 */

import java.util.Scanner;

public class Task_10 {
    public static void main(String[] args) {
        PlayGame();

    }
    public static boolean PlayGame(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ваша задача угадать число.");
        int range = 1000;
         int number = (int) (Math.random() * range);
         boolean a = true;
         while (a){
             System.out.println("Угадайте число от 0 до "+ range);
           int  m= scanner.nextInt();
             if (m == number){
                 System.out.println("Победа");
                 break;
             }else if (m > 0 && m < number){
                 System.out.println("Это число меньше загаданного.");


             } else if (m > number) {
                 System.out.println("Это число больше загаданного.");


             } else if(m < 0) {
                 a = false;
                 break;

             }

         }
        return a;
    }
}

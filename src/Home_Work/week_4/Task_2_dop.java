package Home_Work.week_4;
// решить седьмую задачу за линейное время
/*
.


На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.

Необходимо создать массив, полученный из исходного возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.

 */

import java.util.Arrays;
import java.util.Scanner;

public class Task_2_dop {
    public static void sortSquares(int arr[]) {
        int n = arr.length, beginOfmassive = 0, endOfMassive = n - 1;

        int result[] = new int[n];

        for(int index = n - 1; index >= 0; index--)
        {
            if (Math.abs(arr[beginOfmassive]) > arr[endOfMassive]) {
                result[index] = arr[beginOfmassive] * arr[beginOfmassive];
                beginOfmassive++;
            }
            else {
                result[index] = arr[endOfMassive] * arr[endOfMassive];
                endOfMassive--;
            }
        }
        for(int i = 0; i < n; i++)
            arr[i] = result[i];
    }

    // Driver code
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        // n равняется длине массива
        int n= scanner.nextInt();
        // создаем массив с длиной n
        int arr[] = new int[n];
        //заполняем массив
        for (int i = 0; i <n ; i++) {
            arr[i]= scanner.nextInt();
        }
         n = arr.length;

        System.out.println("До входа в метод  sortSquares ");
        for(int i = 0; i < n; i++)
            System.out.print(arr[i] + " ");

        sortSquares(arr);
        System.out.println("");
        System.out.println("После выхода из него ");
        for(int i = 0; i < n; i++)
            System.out.print(arr[i] + " ");
    }
}













package Second_week_For_Practice;
/*
Дано число n. Четное оно или нет.
 */

import java.util.Scanner;

public class Task_1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        String str;
        // тернальный оператор как альтернатива if-else
        str = (n % 2 == 0) ? "число четное" : "Число не четное";
        System.out.println(str);
    }
}


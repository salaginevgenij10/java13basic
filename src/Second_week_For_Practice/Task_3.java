package Second_week_For_Practice;
/*
Проверка числа на полиндром. Число 4-х значное.
 */

import java.util.Scanner;

public class Task_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n > 999 && n < 10000) {
            int end = n % 10;
            int start = n / 1000;

            if (end != start) {
                System.out.println("не палиндром");
            }
            else {
                end = (n % 100) / 10;
                start = (n / 100) % 10;
                if (end != start) {
                    System.out.println("не палиндром");
                }
                else {
                    System.out.println("палиндром");
                }
            }
        }
        else {
            System.out.println("Ввели некорректное число (4 знака должно быть)");
        }
    }
}


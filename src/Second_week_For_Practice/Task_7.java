package Second_week_For_Practice;
/*
Реализовать System.out.println(), используя System.out.print() и табуляцию \n
Входные данные: два слова, считываемые из консоли

Входные данные
Hello World
Выходные данные
Hello
World

 */


import java.util.Scanner;

public class Task_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String one = scanner.nextLine();
        String two = scanner.nextLine();
        System.out.print(one + "\n" + two);
    }
}

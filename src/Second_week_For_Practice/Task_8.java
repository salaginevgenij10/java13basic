package Second_week_For_Practice;
/*
Дан символ, поменять со строчного на заглавный или с заглавного на строчный

Входные данные
d
Выходные данные
D
Входные данные
A
Выходные данные
a
 */


import java.util.Locale;
import java.util.Scanner;

public class Task_8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        /*String one = scanner.nextLine();
        System.out.println(one.toUpperCase());
        System.out.println(one.toLowerCase());*/
        String symbol=scanner.next();
        char c = symbol.charAt(0);
        if (c>='a' && c<='z'){
            System.out.println((char) (c+('A'-'a')));
        }else {
            System.out.println((char) (c-('A'-'a')));
        }
    }
}

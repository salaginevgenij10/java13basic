package Second_week_For_Practice;
/*
   Дана строка и паттерн, заменить паттерн на паттерн, состоящий из заглавных символов
   Входные данные
   Hello
   o
   Выходные данные
   HellO

   Входные данные
   Hello world
   ld
   Выходные данные
   Hello worLD
    */


import java.util.Scanner;

public class Task_9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String string = scanner.nextLine();
        String pattern = scanner.next();

        String upperPattern = pattern.toUpperCase();
        System.out.println(string.replace(pattern,upperPattern));
    }
}
